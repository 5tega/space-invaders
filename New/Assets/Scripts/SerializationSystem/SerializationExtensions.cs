﻿using System.IO;
using UnityEngine;

namespace SerializationSystem
{
    public static class SerializationExtensions
    {
        public static void WriteBytes(this in Color color, BinaryWriter writer)
        {
            writer.Write(color.r);
            writer.Write(color.g);
            writer.Write(color.b);
            writer.Write(color.a);
        }

        public static void ReadBytes(this ref Color destination, BinaryReader reader)
        {
            destination.r = reader.ReadSingle();
            destination.g = reader.ReadSingle();
            destination.b = reader.ReadSingle();
            destination.a = reader.ReadSingle();
        }

        public static void WriteBytes(this in Vector2 val, BinaryWriter writer)
        {
            writer.Write(val.x);
            writer.Write(val.y);
        }

        public static void ReadBytes(this ref Vector2 dest, BinaryReader reader)
        {
            dest.x = reader.ReadSingle();
            dest.y = reader.ReadSingle();
        }
    }
}