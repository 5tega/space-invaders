using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BattleSettings", menuName = "ScriptableObjects/BattleSettings")]
public class BattleSettings : ScriptableObjectSingleton<BattleSettings>
{
    public float SpaceshipSpeed = 1f;
    public int SpaceshipHealth = 100;
    public int SpaceshipDamage = 1;
}
