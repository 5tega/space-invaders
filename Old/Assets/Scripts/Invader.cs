﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Invader : MonoBehaviour
{
    // How many rows of this alien should be spawned?
    public int rows;
    private int animFrame = 0;
    private void Awake() {
        // Connect to manager tick
        GameManager.Instance.OnMoveTick += tick;
        GetComponent<Animator>().speed = 0;
    }

    public void tick(MoveDirection moveDirection)
    {
        if (animFrame == 0)
        {
            animFrame = 1;
        }
        else
        {
            animFrame = 0;
        }
        GetComponent<Animator>().SetInteger("Frame", animFrame);
        switch (moveDirection) 
        {
            case MoveDirection.DOWN:
                downStep();
            break;

            case MoveDirection.LEFT:
                sideStep(-1);
            break;

            case MoveDirection.RIGHT:
                sideStep(1);
            break;
        }
    }

    public void sideStep(int moveDirection)
    {
        transform.position += new Vector3(moveDirection*GameManager.Instance.stepLength, 0, 0);
    }

    public void downStep()
    {
        transform.position += new Vector3(0, -GameManager.Instance.stepLength, 0);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "PlayerProjectile")
        {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }

    private void OnDestroy() {
        // Disconnect from delegate
        GameManager.Instance.OnMoveTick -= tick;
    }
}
